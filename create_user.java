package ua.org.autotest;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

import static ua.org.autotest.poker_player.generate_player;

public class create_user {

    @BeforeSuite
    void setup()
    {
        WebDriverManager.chromedriver().setup();
        WebDriver web_driver = new ChromeDriver();

        tester_ = new poker_game_tester(web_driver);
    }

    @BeforeTest
    public void login()
    {
        poker_game_tester.website_credentials credentials = new poker_game_tester.website_credentials();
        credentials.login = site_config.login;
        credentials.password = site_config.password;

        tester_.open_website("http://qa-poker.teaminternational.com");
        tester_.login_user(credentials);
    }

    @Test
    public void create_user_test()
    {
        poker_player player = generate_player();

        tester_.add_user(player);
        tester_.find_user(player);
        tester_.validate_user_fields(player);
        tester_.delete_user(player);
    }

    @AfterTest
    void logout(){ tester_.logout();
    }

    @AfterSuite
    void close_browser()
    {
        tester_.close_browser();
    }

    private poker_game_tester tester_;
}
