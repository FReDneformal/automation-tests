package ua.org.autotest;

public class main
{
    public static void main(String[] args)
    {
        poker_game_tester tester = new poker_game_tester();

        poker_player player = new poker_player();

        player.login = poker_player.generate_random_name();
        player.email = poker_player.generate_random_name() + "@gmail.com";
        player.password_confirmed = "12345QwE";
        player.first_name = "Ivan";
        player.last_name = "Ivanov";
        player.city = "Kharkov";
        player.address = "panasivska street apartment 54";
        player.phone = "0988765432";

        poker_game_tester.website_credentials credentials = new poker_game_tester.website_credentials();
        credentials.login = "admin";
        credentials.password = "test";

        tester.open_website("http://qa-poker.teaminternational.com");
        tester.login_user(credentials);
        tester.add_user(player);
        tester.validate_user_fields(player);

        poker_player edited_player = new poker_player();

        edited_player.login = player.login;
        edited_player.email = poker_player.generate_random_name() + "@gmail.com";
        edited_player.password_confirmed = "12345QwE";
        edited_player.first_name = "Petrov";
        edited_player.last_name = "Petrov";
        edited_player.city = "Kiev";
        edited_player.address = "ivanova street apartment 88";
        edited_player.phone = "0991234567";

        tester.edit_user(edited_player);
        tester.validate_user_fields(edited_player);
    }
}
